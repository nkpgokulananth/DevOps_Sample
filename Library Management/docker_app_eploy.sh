#!/bin/bash

p_img_name=$1
container_name=$1
p_service_name=$2
p_port="22"$3
static_port=2252
p_app_cntxt_path=$4

curr_dir=$(pwd)
cp /home/osgdev/VE844077/Projects/LibraryManagement/openjdk-9.0.4_linux-x64_bin.tar.gz $curr_dir/
cp /home/osgdev/VE844077/Projects/LibraryManagement/apache-tomcat-9.0.27.tar.gz $curr_dir/

echo "Started deploying the application."
echo "##################################"

docker container prune --force

swarm_status=$(docker swarm leave --force 2>&1)
#Node left the swarm.
if echo "$swarm_status"|grep -iq "Node left the swarm."; then 
	echo $swarm_status
	echo "Exited from swarm"
#elif echo "$swarm_status"|grep -iq "ERROR"; then
else
	echo $swarm_status
	exit 
fi

doc_swarm_status=$(docker swarm init 2>&1)
if echo "$doc_swarm_status"|grep -iq "To add a"; then 
	echo "Swarm is created"
	echo $doc_swarm_status
#elif echo "$doc_swarm_status"|grep -iq "ERROR"; then
else
	echo "Error in creating in swarm"
	echo $doc_swarm_status 
	exit
fi

service_exist=$(docker service list|grep $p_service_name|awk 'BEGIN {FS=" "};{print $2}' 2>&1)
echo $service_exist

if [ "$service_exist"=="$p_service_name" ] 
then
	doc_service_status=$(docker service remove $service_exist)
	echo "Docker service removed:$doc_service_status"
fi

container_ids=$(docker container list -a|grep $container_name| awk 'BEGIN {FS=" "}; {print $1}')
for i in $container_ids
do
	echo "container is for $container_name is: $i"
	container_stop_status=$(docker container stop $i 2>&1)
done

prune_status=$(docker container prune --force 2>&1)
if echo "$prune_status"|grep -iq "Total reclaimed space:"; then 
	echo $prune_status
	echo "pruned the stoped containers"
#elif echo "$prune_status"|grep -iq "ERROR"; then
else
	echo "Errer in pruning the stoped containers"
	echo $prune_status 
	exit
fi


image_name=$(docker image ls|grep $p_img_name| awk 'BEGIN {FS=" "};{print $1}')

if [ "$image_name"=="$p_img_name" ] 
then
	img_rm_status=$(docker image rm $image_name 2>&1)
	echo "Existing image is removed."
	echo $img_rm_status
fi

img_build_status=$(docker image build -t libmanagement . 2>&1)
if echo "$img_build_status"|grep -iq "Successfully built"; then 
	echo "Image is created."
	echo $img_build_status
#elif echo "$img_build_status"|grep -iq "ERROR"; then
else
	echo "Error in image creation."
	echo $img_build_status 
	exit;
fi


doc_service_create_status=$(docker service create --name $p_service_name --replicas 2 --publish $static_port:8080 $p_img_name 2>&1)

if echo "$doc_service_create_status"|grep -iq "verify: Service converge"; then 
	echo "Service is created"
	echo $doc_service_create_status
#elif echo "$doc_service_create_status"|grep -iq "ERROR"; then
else
	echo "Error in creating in service"
	echo $doc_service_create_status 
	exit
fi

host_ip=$(hostname  -I| awk 'BEGIN {FS=" "};{print $1}')
echo "*********************************************************"
echo "* Application deployed successfully on the following URL*"
echo "* http://$host_ip:$static_port/$p_app_cntxt_path             *"
echo "*********************************************************"

#echo -e "Test"| mail -r venkat.sibbyala@gmail.com -s "Test" venkat.sibbyala@gmail.com
