#!/bin/bash

p_img_name=$1
container_name=$1
p_service_name=$2

curr_dir=$(pwd)
cp /home/osgdev/JO323014/LibraryManagement/openjdk-9.0.4_linux-x64_bin.tar.gz $curr_dir/ansible/
cp /home/osgdev/JO323014/LibraryManagement/apache-tomcat-9.0.27.tar.gz $curr_dir/ansible/

echo "################################## Started cleaning docker objects.##################################"


service_exist=$(docker service list|grep $p_service_name|awk 'BEGIN {FS=" "};{print $2}' 2>&1)
echo "Removing the $p_service_name service if exist"

if [ ${#service_exist} -gt 2 ] 
then
	doc_service_status=$(docker service remove $p_service_name)
	echo "Docker service removed:$doc_service_status"
fi

container_ids=$(docker container list -a|grep $container_name| awk 'BEGIN {FS=" "}; {print $1}')
for i in $container_ids
do
	echo "container is for $container_name is: $i"
	container_stop_status=$(docker container stop $i 2>&1)
done

image_name=$(docker image ls|grep $p_img_name| awk 'BEGIN {FS=" "};{print $1}')
echo "Removing the $p_img_name image if found"
if [ ${#image_name} -gt 2 ] 
then
	img_rm_status=$(docker image rm $image_name 2>&1)
	echo "Existing image is removed."
	echo $img_rm_status
fi
echo "Pruning all docker objects."
docker image prune --force
docker container prune --force
echo "Pruning done for all docker objects."
echo "Leaving from existing swarm. if already present"
swarm_status=$(docker swarm leave --force 2>&1)
#Node left the swarm.
if echo "$swarm_status"|grep -iq "Node left the swarm."; then 
	echo $swarm_status
	echo "Exited from swarm"
fi


echo "################################## Completed cleaning docker objects.##################################"
