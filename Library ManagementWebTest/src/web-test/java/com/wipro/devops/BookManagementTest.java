package com.wipro.devops;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import org.openqa.selenium.firefox.FirefoxOptions;

import com.wipro.devops.WebTest;

import org.junit.*;
import static org.junit.Assert.*;

import java.io.File;

import javax.sound.midi.SysexMessage;

import org.junit.experimental.categories.Category;


@Category(WebTest.class)
public class BookManagementTest {

	static WebDriver driver;

	@BeforeClass
	public static void setup() {
	//	driver = new ChromeDriver();
		// new FirefoxDriver();
				FirefoxBinary firefoxBinary = new FirefoxBinary();
        firefoxBinary.addCommandLineOptions("--headless");
        System.setProperty("webdriver.gecko.driver", "/usr/bin/geckodriver");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setBinary(firefoxBinary);
        
        driver = new FirefoxDriver(firefoxOptions);
	}

	@AfterClass
	public static void cleanUp() {
		driver.quit();
	}
	/*@Test
	public void loginLibrarianSuccess() {
		driver.get("http://10.199.0.71:2252/LibraryManagement/LibrarianLogin");
		WebElement email = driver.findElement(By.name("email"));
		WebElement pass = driver.findElement(By.name("password"));
		WebElement admbutton = driver.findElement(By.className("btn-primary"));         
		email.sendKeys("venkat@wipro.com");
		pass.sendKeys("123");
		admbutton.click();
		System.out.println("Source Message "+driver.getPageSource()+"=============");
		assertTrue(driver.getPageSource().contains("Book"));
	}*/
	@Test
	public void loginLibrarianFailure() {
		driver.get("http://localhost:5050/LibraryManagement");
		WebElement email = driver.findElement(By.name("email"));
		WebElement pass = driver.findElement(By.name("password"));
		WebElement admbutton = driver.findElement(By.name("lsubmit"));         
		email.sendKeys("venkat@wipro.com");
		pass.sendKeys("123erqdcas");
		admbutton.click();
		assertTrue(driver.getPageSource().contains("error"));
	}
	/*@Test
	public void loginLibrarianSuccess() {
        driver.get("http://10.199.0.71:2252/LibraryManagement");
        WebElement email = driver.findElement(By.name("lemail"));
        WebElement pass = driver.findElement(By.name("lpassword"));
        WebElement libsubmit = driver.findElement(By.name("libsubmit"));         
        email.sendKeys("venkat@wipro.com");
        pass.sendKeys("123");
        libsubmit.click();
        System.out.println(driver.getPageSource());
        assertTrue(driver.getPageSource().contains("Book"));
	}
	
	@Test
	public void loginLibrarianFail() {
        driver.get("http://10.199.0.71:2252/LibraryManagement");
        WebElement email = driver.findElement(By.name("lemail"));
        WebElement pass = driver.findElement(By.name("lpassword"));
        WebElement libsubmit = driver.findElement(By.name("libsubmit"));           
        email.sendKeys("venkat@wipro.com");
        pass.sendKeys("123456");
        libsubmit.click();
        assertTrue(driver.getPageSource().contains("error"));
	}
	
	
	@Test
	public void addLibrarian() {
        driver.get("http://localhost:5050/LibraryManagement/AddLibrarianForm");
        WebElement name = driver.findElement(By.name("name"));
        WebElement email = driver.findElement(By.name("email"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement mobile = driver.findElement(By.name("mobile"));
        WebElement button = driver.findElement(By.name("submit"));         
        name.sendKeys("vsibbyal");
        email.sendKeys("vsibbyal@gmail.com");
        password.sendKeys("123");
        mobile.sendKeys("123");
        button.click();
        assertTrue(driver.getPageSource().contains("successfully"));
	}*/
	/*
	//@Test
	public void loginFail() {
        driver.get("http://localhost:6080/ILP_Bookstore");
        WebElement email = driver.findElement(By.name("email"));
        WebElement pass = driver.findElement(By.name("password"));
        WebElement button = driver.findElement(By.xpath("/html/body/form/div/button"));         
        email.sendKeys("avinash.patel@wipro.com");
        pass.sendKeys("1234566666666");
        button.click();
        assertTrue(driver.getPageSource().contains("Invalid username or password, Please try again with valid"));
	}*/
	
	/*@Test
	public void registrationSuccess() {
        driver.get("http://localhost:5050/DevOpsWiproBookStore/register");
        WebElement username = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement email = driver.findElement(By.name("email"));
        WebElement firstname = driver.findElement(By.name("firstname"));
        WebElement button = driver.findElement(By.name("register"));      
        username.sendKeys("VENKAT");
        password.sendKeys("VENKAT");
        email.sendKeys("1234@wipro.com");
        firstname.sendKeys("VENKAT RAMANA");
        button.click();
        //System.out.println("Source Message "+driver.getPageSource());
        assertTrue(driver.getPageSource().contains("VENKAT RAMANA"));
	}
	
	@Test
	public void forgotPasswordSuccess() {
        driver.get("http://localhost:5050/DevOpsWiproBookStore/reset");      
        WebElement username = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement cnfpassword = driver.findElement(By.name("cnfpassword"));
        WebElement button = driver.findElement(By.name("Reset"));      
        username.sendKeys("vsibbyal");
        password.sendKeys("1234");
        cnfpassword.sendKeys("1234");
        button.click();
        assertTrue(driver.getPageSource().contains("password successfully reset"));
	}
	*/

}

