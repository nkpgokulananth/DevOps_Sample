package com.wipro.devops;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import org.openqa.selenium.firefox.FirefoxOptions;

import com.wipro.devops.WebTest;

import org.junit.*;
import static org.junit.Assert.*;

import java.io.File;

import javax.sound.midi.SysexMessage;

import org.junit.experimental.categories.Category;


@Category(WebTest.class)
public class LibraryManagementTest {

	static WebDriver driver;

	@BeforeClass
	public static void setup() {
	//	driver = new ChromeDriver();
		// new FirefoxDriver();
				FirefoxBinary firefoxBinary = new FirefoxBinary();
        firefoxBinary.addCommandLineOptions("--headless");
        System.setProperty("webdriver.gecko.driver", "/usr/bin/geckodriver");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setBinary(firefoxBinary);
        
        driver = new FirefoxDriver(firefoxOptions);
	}

	@AfterClass
	public static void cleanUp() {
		driver.quit();
	}
	@Test
	public void loginAdminSuccess() {
        driver.get("http://localhost:5050/LibraryManagement");
        WebElement email = driver.findElement(By.name("email"));
        WebElement pass = driver.findElement(By.name("password"));
        WebElement admbutton = driver.findElement(By.name("asubmit"));         
        email.sendKeys("venkat@wipro.com");
        pass.sendKeys("venkat");
        admbutton.click();
        assertTrue(driver.getPageSource().contains("Admin Section"));
	}
	
	@Test
	public void loginAdminFail() {
        driver.get("http://localhost:5050/LibraryManagement");
        WebElement email = driver.findElement(By.name("email"));
        WebElement pass = driver.findElement(By.name("password"));
        WebElement admbutton = driver.findElement(By.name("asubmit"));           
        email.sendKeys("venkat@wipro.com");
        pass.sendKeys("123asdfzxcv");
        admbutton.click();
	//System.out.println("Source Message "+driver.getPageSource()+"=============");
        assertTrue(driver.getPageSource().contains("error"));
	}
	
	
	@Test
	public void addLibrarian() {
        driver.get("http://localhost:5050/LibraryManagement/AddLibrarianForm");
        WebElement name = driver.findElement(By.name("name"));
        WebElement email = driver.findElement(By.name("email"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement mobile = driver.findElement(By.name("mobile"));
        WebElement button = driver.findElement(By.className("btn-primary"));         
        name.sendKeys("vsibbyal");
        email.sendKeys("vsibbyal@gmail.com");
        password.sendKeys("123");
        mobile.sendKeys("123");
        button.click();
        assertTrue(driver.getPageSource().contains("successfully"));
	}

}

